package com.flipcart.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.flipcart.base.BaseClass;
import com.flipcart.pages.LicensePage;

public class License extends BaseClass {

	@BeforeTest
	public void report() 
	{
		BaseClass.startReport();
	}
	
	@BeforeMethod
	public void setupDriverServer() 
	{
		try {
			BaseClass.setup();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void license() throws Exception 
	{
		test = extent.createTest("License");
				
		LicensePage licensePage = new LicensePage();
		
		BaseClass.preSettingsForApp(driver,readConfig.getLangauge());
		
		loginpage.enterUsername(driver, readConfig.getUsername());
		loginpage.clickOnSubmit(driver);
		loginpage.enterPassword(driver, readConfig.getPassword());
		loginpage.clickOnSubmit(driver);
		
		
		licensePage.clickOnImageButton(driver);
		test.log(Status.INFO, "clicked on image button.");
		
		licensePage.clickOnLegal(driver);
		test.log(Status.INFO, "clicked on Legal.");
		
		licensePage.clickOnPolicies(driver);
		test.log(Status.INFO, "clicked on policies.");
	}
	
	@AfterMethod
	public void tearDown(ITestResult result) 
	{
		BaseClass.close(result);
	}
	
	@AfterTest
	public void closeReport() 
	{
		BaseClass.endReport();
	}
	
}
