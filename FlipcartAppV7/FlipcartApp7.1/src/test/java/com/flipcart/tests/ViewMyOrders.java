package com.flipcart.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.flipcart.base.BaseClass;
import com.flipcart.pages.ViewMyOrdersPage;

public class ViewMyOrders extends BaseClass {
	
	@BeforeTest
	public void report() 
	{
		BaseClass.startReport();
	}
	
	@BeforeMethod
	public void setupDriverServer() 
	{
		try {
			BaseClass.setup();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void ViewOrders() throws Exception 
	{
		test = extent.createTest("View My Orders");
		
		ViewMyOrdersPage viewOrderPage = new ViewMyOrdersPage();
	
		BaseClass.preSettingsForApp(driver,readConfig.getLangauge());
		
		loginpage.enterUsername(driver, readConfig.getUsername());
		loginpage.clickOnSubmit(driver);
		loginpage.enterPassword(driver, readConfig.getPassword());
		loginpage.clickOnSubmit(driver);
		
		viewOrderPage.clickOnImageButton(driver);
		test.log(Status.INFO, "Clicked on setting Image Button.");
		
		viewOrderPage.clickOnMyOrders(driver);
		test.log(Status.INFO, "Clicked on my orders.");
		
		viewOrderPage.searchYourOrder(driver, readConfig.getOrderName());
		test.log(Status.INFO, "Searched your order.");
	}
	
	@AfterMethod
	public void tearDown(ITestResult result) 
	{
		BaseClass.close(result);
	}
	
	@AfterTest
	public void closeReport() 
	{
		BaseClass.endReport();
	}
	
}
