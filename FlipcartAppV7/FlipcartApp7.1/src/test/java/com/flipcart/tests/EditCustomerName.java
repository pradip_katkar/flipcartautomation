package com.flipcart.tests;


import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.flipcart.base.BaseClass;
import com.flipcart.pages.EditCustNamePage;

public class EditCustomerName extends BaseClass {
	
	@BeforeTest
	public void report() 
	{
		BaseClass.startReport();
	}
	
	@BeforeMethod
	public void setupDriverServer() 
	{
		try {
			BaseClass.setup();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void EditName() throws Exception 
	{
		test = extent.createTest("Edit Customer Name");
		
		EditCustNamePage editNamePage = new EditCustNamePage();	

		BaseClass.preSettingsForApp(driver,readConfig.getLangauge());
		
		loginpage.enterUsername(driver, readConfig.getUsername());
		loginpage.clickOnSubmit(driver);
		loginpage.enterPassword(driver, readConfig.getPassword());
		loginpage.clickOnSubmit(driver);
		
		editNamePage.clickOnImageButton(driver);
		test.log(Status.INFO, "Clicked on setting Image Button.");
		
		editNamePage.clickOnMyAccount(driver);
		test.log(Status.INFO, "Clicked on my account.");
		
		editNamePage.clickAccountSetings(driver);
		test.log(Status.INFO, "Clicked on account setings.");
		
		editNamePage.putFirstName(driver,readConfig.getFirstName());
		test.log(Status.INFO, "Edited first name.");
		
		editNamePage.putLastName(driver,readConfig.getLastName());
		test.log(Status.INFO, "Edited last name.");
		
		editNamePage.clickOnSubmit(driver);
		test.log(Status.INFO, "Clicked on submit button.");
	}
	
	
	@AfterMethod
	public void tearDown(ITestResult result) 
	{
		BaseClass.close(result);
	}
	
	@AfterTest
	public void closeReport() 
	{
		BaseClass.endReport();
	}
	
}
