package com.flipcart.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.flipcart.base.BaseClass;
import com.flipcart.pages.LoginPage;
import com.flipcart.pages.UncheckNotificationPage;

public class UncheckNotification extends BaseClass {

	@BeforeTest
	public void report() 
	{
		BaseClass.startReport();
	}
	
	@BeforeMethod
	public void setupDriverServer() 
	{
		try {
			BaseClass.setup();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void Notification() throws Exception 
	{
		test = extent.createTest("Notification Preferences"); 
		
		UncheckNotificationPage notification = new UncheckNotificationPage();	

		BaseClass.preSettingsForApp(driver,readConfig.getLangauge());
		
		LoginPage loginpage = new LoginPage();
		
		loginpage.enterUsername(driver, readConfig.getUsername());
		loginpage.clickOnSubmit(driver);
		loginpage.enterPassword(driver, readConfig.getPassword());
		loginpage.clickOnSubmit(driver);
		
		notification.clickOnImageButton(driver);
		test.log(Status.INFO, "Clicked on setting Image Button.");
		
		notification.clickOnNotificationPreferences(driver);
		test.log(Status.INFO, "click On Notification Preferences.");
		
		notification.clickOnFeedback(driver);
		test.log(Status.INFO, "Check | uncheck checkbox.");
	}
	
	@AfterMethod
	public void tearDown(ITestResult result) 
	{
		BaseClass.close(result);
	}
	
	@AfterTest
	public void closeReport() 
	{
		BaseClass.endReport();
	}
	
}
