package com.flipcart.tests;

import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.flipcart.base.BaseClass;
import com.flipcart.pages.AddToCartPage;
import com.flipcart.pages.SearchingItemPage;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;


public class AddToCart extends BaseClass {
	
	@BeforeTest
	public void report() 
	{
		BaseClass.startReport();
	}
	
	@BeforeMethod
	public void setupDriverServer() 
	{
		try {
			BaseClass.setup();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void addToCart() throws Exception 
	{
		test = extent.createTest("Add To Cart Test"); 
		
		AddToCartPage addtocartpage = new AddToCartPage();
			
		BaseClass.preSettingsForApp(driver,readConfig.getLangauge());
		
		loginpage.enterUsername(driver, readConfig.getUsername());
		loginpage.clickOnSubmit(driver);
		loginpage.enterPassword(driver, readConfig.getPassword());
		loginpage.clickOnSubmit(driver);
		
		SearchingItemPage searchingItemPage = new SearchingItemPage();
		
		searchingItemPage.getProductName(driver, readConfig.getSearchProductName());
		//addtocartpage.getProductName(driver, readConfig.getSearchProductName());
		test.log(Status.INFO, "Product category entered.");
		
		searchingItemPage.clickOnProductCategory(driver);
		//addtocartpage.clickOnProductCategory(driver);
		test.log(Status.INFO, "Clicked on product category entered.");
		
		searchingItemPage.clickOnAllowPermissions(driver);
		//addtocartpage.clickOnAllowPermissions(driver);
		test.log(Status.INFO, "Clicked on allow permissions.");
		
		searchingItemPage.searchProduct(driver);
		//addtocartpage.searchProduct(driver);
		test.log(Status.INFO, "Product searched successfully.");
		
		addtocartpage.clickAddToCart(driver);
		test.log(Status.INFO, "Clicked on add product to cart.");
		
		//addtocartpage.selectAddress(driver);
		//test.log(Status.INFO, "address selected.");
		
		addtocartpage.clickAddToCart(driver);
		test.log(Status.INFO, "Product Added to cart successfully.");
		
		addtocartpage.goToCart(driver);
		test.log(Status.INFO, "Clicked on Go To Cart.");
	}
	
	@AfterMethod
	public void tearDown(ITestResult result) 
	{
		BaseClass.close(result);
	}
	
	@AfterTest
	public void closeReport() 
	{
		BaseClass.endReport();
	}
}
