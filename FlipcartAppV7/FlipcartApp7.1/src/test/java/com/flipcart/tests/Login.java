package com.flipcart.tests;

import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.flipcart.base.BaseClass;

import java.util.concurrent.TimeUnit;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

public class Login extends BaseClass {

	@BeforeTest
	public void report() 
	{
		BaseClass.startReport();
	}
	
	@BeforeMethod
	public void setupDriverServer() 
	{
		try {
			BaseClass.setup();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void loginTest() throws Exception 
	{
		test = extent.createTest("Login Test");
			
		BaseClass.preSettingsForApp(driver,readConfig.getLangauge());
		
		loginpage.enterUsername(driver, readConfig.getUsername());
		test.log(Status.INFO, "Username entered.");
		
		loginpage.clickOnSubmit(driver);
		test.log(Status.INFO, "Clicked on Submit Button.");
		
		loginpage.enterPassword(driver, readConfig.getPassword());
		test.log(Status.INFO, "Password entered.");
		
		loginpage.clickOnSubmit(driver);
		test.log(Status.INFO, "Clicked on Submit Button.");
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.MINUTES);
	}
	
	@AfterMethod
	public void tearDown(ITestResult result) 
	{
		BaseClass.close(result);
	}
	
	@AfterTest
	public void closeReport() 
	{
		BaseClass.endReport();
	}
	
}
