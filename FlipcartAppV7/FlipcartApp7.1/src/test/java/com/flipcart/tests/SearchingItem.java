package com.flipcart.tests;

import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.flipcart.base.BaseClass;
import com.flipcart.pages.SearchingItemPage;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

public class SearchingItem extends BaseClass {
	
	@BeforeTest
	public void report() 
	{
		BaseClass.startReport();
	}
	
	@BeforeMethod
	public void setupDriverServer() 
	{
		try {
			BaseClass.setup();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void searchItem() throws Exception {	
		
		test = extent.createTest("Search Item Test");
		
		SearchingItemPage searchpage = new SearchingItemPage();
		
		BaseClass.preSettingsForApp(driver,readConfig.getLangauge());
		
		loginpage.enterUsername(driver, readConfig.getUsername());
		loginpage.clickOnSubmit(driver);
		loginpage.enterPassword(driver, readConfig.getPassword());
		loginpage.clickOnSubmit(driver);
		
		searchpage.getProductName(driver, readConfig.getSearchProductName());
		test.log(Status.INFO, "Product category entered.");
		
		searchpage.clickOnProductCategory(driver);
		test.log(Status.INFO, "Clicked on product category entered.");
		
		searchpage.clickOnAllowPermissions(driver);
		test.log(Status.INFO, "Clicked on allow permissions.");
		
		searchpage.searchProduct(driver);	
		test.log(Status.INFO, "Product searched successfully.");
	}

	@AfterMethod
	public void tearDown(ITestResult result) 
	{
		BaseClass.close(result);
	}
	
	@AfterTest
	public void closeReport() 
	{
		BaseClass.endReport();
	}
}
