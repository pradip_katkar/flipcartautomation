package com.flipcart.tests;

import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.flipcart.base.BaseClass;
import com.flipcart.pages.LogoutPage;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

public class Logout extends BaseClass {

	
	@BeforeTest
	public void report() 
	{
		BaseClass.startReport();
	}
	
	@BeforeMethod
	public void setupDriverServer() 
	{
		try {
			BaseClass.setup();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void logoutTest() throws Exception 
	{
		test = extent.createTest("Logout Test");
		
		LogoutPage logoutpage = new LogoutPage();

		BaseClass.preSettingsForApp(driver,readConfig.getLangauge());
		
		loginpage.enterUsername(driver, readConfig.getUsername());
		loginpage.clickOnSubmit(driver);
		loginpage.enterPassword(driver, readConfig.getPassword());
		loginpage.clickOnSubmit(driver);
		
		logoutpage.clickOnImageButton(driver);
		test.log(Status.INFO, "Clicked on setting Image Button ");
		
		logoutpage.clickOnMyAccount(driver);
		test.log(Status.INFO, "Clicked on my account.");
		
		logoutpage.clickOnLogout(driver);
		test.log(Status.INFO, "Clicked on Logout.");
	}
	
	@AfterMethod
	public void tearDown(ITestResult result) 
	{
		BaseClass.close(result);
	}
	
	@AfterTest
	public void closeReport() 
	{
		BaseClass.endReport();
	}
	
	
	
}
