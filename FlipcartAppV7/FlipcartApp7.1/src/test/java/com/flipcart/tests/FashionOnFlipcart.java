package com.flipcart.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.flipcart.base.BaseClass;
import com.flipcart.pages.FashionOnFlipcartPage;

public class FashionOnFlipcart extends BaseClass {
	
	@BeforeTest
	public void report() 
	{
		BaseClass.startReport();
	}
	
	@BeforeMethod
	public void setupDriverServer() 
	{
		try {
			BaseClass.setup();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void fashion() throws Exception
	{
		test = extent.createTest("Fashion Testcase"); 
		
		BaseClass.preSettingsForApp(driver,readConfig.getLangauge());
		
		loginpage.enterUsername(driver, readConfig.getUsername());
		loginpage.clickOnSubmit(driver);
		loginpage.enterPassword(driver, readConfig.getPassword());
		loginpage.clickOnSubmit(driver);
		
		FashionOnFlipcartPage fashionOnFlipcartPage = new FashionOnFlipcartPage();
		
		fashionOnFlipcartPage.clickOnBanner(driver);
		test.log(Status.PASS, "Clicked on All Categories.");
		
		fashionOnFlipcartPage.clickOnInsurance(driver);
		test.log(Status.PASS, "Clicked on Fashion.");
		
	}
	
	@AfterMethod
	public void tearDown(ITestResult result) 
	{
		BaseClass.close(result);
	}
	
	@AfterTest
	public void closeReport() 
	{
		BaseClass.endReport();
	}
	
}
