package com.flipcart.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.flipcart.base.BaseClass;
import com.flipcart.pages.MyCouponsPage;

public class MyCoupons extends BaseClass  {

	@BeforeTest
	public void report() 
	{
		BaseClass.startReport();
	}
	
	@BeforeMethod
	public void setupDriverServer() 
	{
		try {
			BaseClass.setup();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void mycoupons() throws Exception 
	{
		test = extent.createTest("My Coupon");
		
		MyCouponsPage mycoupon = new MyCouponsPage();

		BaseClass.preSettingsForApp(driver,readConfig.getLangauge());
		
		loginpage.enterUsername(driver, readConfig.getUsername());
		loginpage.clickOnSubmit(driver);
		loginpage.enterPassword(driver, readConfig.getPassword());
		loginpage.clickOnSubmit(driver);
		
		
		mycoupon.clickOnImageButton(driver);
		test.log(Status.INFO, "clicked on image button.");
		
		mycoupon.clickOnMyRewards(driver);
		test.log(Status.INFO, "clicked on my coupons.");
		
		mycoupon.clickOnCoupons(driver);
		test.log(Status.INFO, "clicked on coupon.");
		
		mycoupon.AllowPermissions(driver);
		test.log(Status.INFO, "Location permissions allowed");
		
		mycoupon.clickOnFirstCoupon(driver);
		test.log(Status.INFO, "clicked on first coupon.");
		
		mycoupon.clickOnAddCouponToCart(driver);
		test.log(Status.INFO, "clicked on add coupon to cart.");
	}
	
	@AfterMethod
	public void tearDown(ITestResult result) 
	{
		BaseClass.close(result);
	}
	
	@AfterTest
	public void closeReport() 
	{
		BaseClass.endReport();
	}
	
}
