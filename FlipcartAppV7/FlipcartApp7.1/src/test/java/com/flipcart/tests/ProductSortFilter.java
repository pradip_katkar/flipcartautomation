package com.flipcart.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.flipcart.base.BaseClass;
import com.flipcart.pages.ProductSortFilterPage;

public class ProductSortFilter extends BaseClass {

	@BeforeTest
	public void report() 
	{
		BaseClass.startReport();
	}
	
	@BeforeMethod
	public void setupDriverServer() 
	{
		try {
			BaseClass.setup();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void gift() throws Exception 
	{
		test = extent.createTest("ProductSortFilter");
		
		ProductSortFilterPage filterPage = new ProductSortFilterPage();
		
		BaseClass.preSettingsForApp(driver,readConfig.getLangauge());
	
		loginpage.enterUsername(driver, readConfig.getUsername());
		loginpage.clickOnSubmit(driver);
		loginpage.enterPassword(driver, readConfig.getPassword());
		loginpage.clickOnSubmit(driver);
		
		filterPage.getProductName(driver,readConfig.getFilterName());
		test.log(Status.INFO, "Enter product name.");
		
		filterPage.clickOnHeadphone(driver);
		test.log(Status.INFO, "clicked on entered product.");
		
		filterPage.clickOnAllowPermissions(driver);
		test.log(Status.INFO, "Location permission allow.");
		
		filterPage.clickOnSort(driver);
		test.log(Status.INFO, "Click on sort button.");
		
		filterPage.clickOnSortbydiscount(driver);
		test.log(Status.INFO, "Product sorted by discount.");
		
		filterPage.clickOnFilter(driver);
		test.log(Status.INFO, "Click on filter button.");
		
		filterPage.clickOnPriceRange(driver);
		test.log(Status.INFO, "Product sorted by price range.");
		
		filterPage.clickOnApply(driver);
		test.log(Status.INFO, "Clicked on apply button.");
	}
	
	@AfterMethod
	public void tearDown(ITestResult result) 
	{
		BaseClass.close(result);
	}
	
	@AfterTest
	public void closeReport() 
	{
		BaseClass.endReport();
	}
	
}
