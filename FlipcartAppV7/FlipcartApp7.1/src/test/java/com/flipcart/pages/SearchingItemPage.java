package com.flipcart.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class SearchingItemPage {
		
	public void getProductName(AndroidDriver<MobileElement> driver,String productsearchname)
		
	{
		driver.findElement(By.id("com.flipkart.android:id/search_widget_textbox")).click();
		
		driver.findElement(By.id("com.flipkart.android:id/search_autoCompleteTextView")).sendKeys(productsearchname);	
	}
	
	public void clickOnProductCategory(AndroidDriver<MobileElement> driver) 
	{
		driver.findElement(By.id("com.flipkart.android:id/txt_title")).click();
	}
	
	public void clickOnAllowPermissions(AndroidDriver<MobileElement> driver) 
	{
		driver.findElement(By.id("com.flipkart.android:id/allow_button")).click();
		
		driver.findElement(By.id("com.android.permissioncontroller:id/permission_allow_foreground_only_button")).click();
	}
	
	public void searchProduct(AndroidDriver<MobileElement> driver)
	{		
	
		driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"SAMSUNG 253 L Frost Free Double Door 3 Star Refrigerator\"))");
		
		driver.findElement(By.xpath("//*[@text='SAMSUNG 253 L Frost Free Double Door 3 Star Refrigerator']")).click();
		
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);	
	}
}
