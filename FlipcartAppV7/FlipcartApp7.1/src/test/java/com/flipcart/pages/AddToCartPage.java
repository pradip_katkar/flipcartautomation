package com.flipcart.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class AddToCartPage {

	public void getProductName(AndroidDriver<MobileElement> driver,String pname) throws Exception 
	{
		driver.findElement(By.id("com.flipkart.android:id/search_widget_textbox")).click();
		
		driver.findElement(By.id("com.flipkart.android:id/search_autoCompleteTextView")).sendKeys(pname);	
	}
	
	public void clickOnProductCategory(AndroidDriver<MobileElement> driver) 
	{
		driver.findElement(By.id("com.flipkart.android:id/txt_title")).click();
	}
	
	public void clickOnAllowPermissions(AndroidDriver<MobileElement> driver) 
	{
		driver.findElement(By.id("com.flipkart.android:id/allow_button")).click();
		
		driver.findElement(By.id("com.android.permissioncontroller:id/permission_allow_foreground_only_button")).click();
	}
	
	public void searchProduct(AndroidDriver<MobileElement> driver)
	{		
		driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"Whirlpool 190 L Direct Cool Single Door 4 Star Refrigerator\"))");
		
		driver.findElement(By.xpath("//*[@text='Whirlpool 190 L Direct Cool Single Door 4 Star Refrigerator']")).click();
	
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);	
	}
	
	
	public void clickAddToCart(AndroidDriver<MobileElement> driver) 
	{
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath("//*[@text='ADD TO CART']")).click();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
	}
	
	@Deprecated
	public void enterPincodeForDelivery(AndroidDriver<MobileElement> driver,String pincode)
	{
		driver.findElement(By.className("android.widget.EditText")).sendKeys(pincode);;
		driver.findElement(By.xpath("//*[@text='Submit']")).click();
	}
	
	@Deprecated
	public void selectAddress(AndroidDriver<MobileElement> driver) 
	{	
		driver.findElement(By.xpath("//*[@text='HOME']")).click();
	}
	
	public void goToCart(AndroidDriver<MobileElement> driver) 
	{	
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
		driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"GO TO CART\"))");
		
		driver.findElement(By.xpath("//*[@text='GO TO CART']")).click();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	}
	
}
