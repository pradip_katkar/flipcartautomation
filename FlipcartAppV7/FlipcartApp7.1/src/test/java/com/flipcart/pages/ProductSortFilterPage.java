package com.flipcart.pages;

import org.openqa.selenium.By;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class ProductSortFilterPage {

	public void getProductName(AndroidDriver<MobileElement> driver,String pname) throws Exception 
	{
		driver.findElement(By.id("com.flipkart.android:id/search_widget_textbox")).click();
		
		driver.findElement(By.id("com.flipkart.android:id/search_autoCompleteTextView")).sendKeys(pname);	
	}
	
	public void clickOnAllowPermissions(AndroidDriver<MobileElement> driver) 
	{
		driver.findElement(By.id("com.flipkart.android:id/allow_button")).click();
		
		driver.findElement(By.id("com.android.permissioncontroller:id/permission_allow_foreground_only_button")).click();
	}
	
	public void clickOnHeadphone(AndroidDriver<MobileElement> driver) {
		driver.findElement(By.id("com.flipkart.android:id/txt_title")).click();	
	}
	
	public void clickOnSort(AndroidDriver<MobileElement> driver) 
	{
		driver.findElement(By.xpath("//*[@text='Sort']")).click();
	}
	
	public void clickOnSortbydiscount(AndroidDriver<MobileElement> driver) 
	{
		driver.findElement(By.xpath("//*[@text='Discount']")).click();
	}
	
	
	public void clickOnFilter(AndroidDriver<MobileElement> driver) 
	{
		driver.findElement(By.xpath("//*[@text='Filter']")).click();
	}
	
	public void clickOnPriceRange(AndroidDriver<MobileElement> driver) 
	{
		driver.findElement(By.xpath("//*[@text='Rs. 999 - Rs. 1498']")).click();
	}
	
	public void clickOnApply(AndroidDriver<MobileElement> driver) 
	{
		driver.findElement(By.xpath("//*[@text='Apply']")).click();
	}
	
}
