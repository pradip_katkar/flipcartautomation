package com.flipcart.pages;

import org.openqa.selenium.By;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class FashionOnFlipcartPage {
	
		public void clickOnBanner(AndroidDriver<MobileElement> driver)
		{
			driver.findElement(By.id("com.flipkart.android:id/banner_image")).click();
		}
		
		
		public void clickOnInsurance(AndroidDriver<MobileElement> driver)
		{
			driver.findElement(By.id("com.flipkart.android:id/tv_card_view_holder")).click();
		}
		
		public void clickOnMensCategory(AndroidDriver<MobileElement> driver)
		{
			driver.findElement(By.id("com.flipkart.android:id/tv_card_view_holder")).click();
		}
		
		public void clickOnClothing(AndroidDriver<MobileElement> driver)
		{
			driver.findElement(By.id("com.flipkart.android:id/tv_card_view_holder")).click();
		}
		
		public void clickedOnTShirt(AndroidDriver<MobileElement> driver)
		{
			driver.findElement(By.id("com.flipkart.android:id/tv_card_view_holder")).click();
		}
		
		public void clickedOnCasualTShirt(AndroidDriver<MobileElement> driver)
		{
			driver.findElement(By.id("com.flipkart.android:id/tv_card_view_holder")).click();
		}
		
		public void clickOnAllowPermissions(AndroidDriver<MobileElement> driver) 
		{
			driver.findElement(By.id("com.flipkart.android:id/allow_button")).click();
			
			driver.findElement(By.id("com.android.permissioncontroller:id/permission_allow_foreground_only_button")).click();
		}
}
