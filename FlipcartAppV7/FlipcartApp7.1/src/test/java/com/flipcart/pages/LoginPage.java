package com.flipcart.pages;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class LoginPage {

	public void enterUsername(AndroidDriver<MobileElement> driver,String gmail) 
	{
		driver.findElement(By.id("com.flipkart.android:id/phone_input")).sendKeys(gmail);
	}
	
	public void enterPassword(AndroidDriver<MobileElement> driver,String pass) 
	{
		driver.findElement(By.xpath("//*[@text='Password']")).sendKeys((pass));
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}
	
	public void clickOnSubmit(AndroidDriver<MobileElement> driver) 
	{
		driver.findElement(By.id("com.flipkart.android:id/button")).click();	
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
	}
}
