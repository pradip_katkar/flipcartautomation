package com.flipcart.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class MyCouponsPage {
	
	public void clickOnImageButton(AndroidDriver<MobileElement> driver) 
	{
		driver.findElement(By.className("android.widget.ImageButton")).click();
	}
	
	public void clickOnMyRewards(AndroidDriver<MobileElement> driver) 
	{
		driver.findElement(By.xpath("//*[@text='My Rewards']")).click();
	}
	
	public void clickOnCoupons(AndroidDriver<MobileElement> driver) 
	{
		//driver.findElement(By.className("android.widget.ImageView")).click();
		driver.findElement(By.xpath("//*[@text='Grocery Savings Pass Offer ']")).click();
		//driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"Grocery Savings Pass Offer\"))");
		//driver.findElement(By.xpath("//*[@text='Add Coupon ']")).click();
	}
	
	public void AllowPermissions(AndroidDriver<MobileElement> driver)
	{
		driver.findElement(By.id("com.flipkart.android:id/allow_button")).click();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.findElement(By.id("com.android.permissioncontroller:id/permission_allow_foreground_only_button")).click();
	}
	
	
	public void clickOnFirstCoupon(AndroidDriver<MobileElement> driver) 
	{
		driver.findElement(By.xpath("//*[@text='Grocery Savings Pass - 3 Months']")).click();
	}
	
	public void clickOnAddCouponToCart(AndroidDriver<MobileElement> driver) 
	{
		driver.findElement(By.xpath("//*[@text='ADD TO CART']")).click();
	}
	
	
}
