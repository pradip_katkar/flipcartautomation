package com.flipcart.pages;

import org.openqa.selenium.By;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class UncheckNotificationPage {
	

	public void clickOnImageButton(AndroidDriver<MobileElement> driver) 
	{
		driver.findElement(By.className("android.widget.ImageButton")).click();
	}
	
	public void clickOnNotificationPreferences(AndroidDriver<MobileElement> driver) 
	{
		driver.findElement(By.xpath("//*[@text='Notification Preferences']")).click();
	}
	
	public void clickOnFeedback(AndroidDriver<MobileElement> driver) 
	{
		driver.findElement(By.xpath("//*[@text='Feedback and Review']")).click();
		
		driver.findElement(By.xpath("//*[@text='Feedback on products']")).click();
		
		driver.findElement(By.xpath("//*[@text='Answer questions by your fellow buyers']")).click();
	}
	

}
