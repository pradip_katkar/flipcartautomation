package com.flipcart.pages;

import org.openqa.selenium.By;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class ViewMyOrdersPage {
	
	public void clickOnImageButton(AndroidDriver<MobileElement> driver) 
	{
		driver.findElement(By.className("android.widget.ImageButton")).click();
	}
	
	public void clickOnMyOrders(AndroidDriver<MobileElement> driver) 
	{
		driver.findElement(By.xpath("//*[@text='My Orders']")).click();
	}
	
	public void searchYourOrder(AndroidDriver<MobileElement> driver,String ordername) {
		driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\""+ordername+"\"))");
		driver.findElement(By.xpath("//*[@text='"+ordername+"']")).click();
		
	}
	
	
}
