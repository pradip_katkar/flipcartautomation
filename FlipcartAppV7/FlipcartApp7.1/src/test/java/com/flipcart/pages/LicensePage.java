package com.flipcart.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class LicensePage {

	public void clickOnImageButton(AndroidDriver<MobileElement> driver) 
	{
		driver.findElement(By.className("android.widget.ImageButton")).click();
	}
	
	public void clickOnLegal(AndroidDriver<MobileElement> driver) 
	{
		driver.findElement(By.xpath("//*[@text='Legal']")).click();
	}
	
	public void clickOnPolicies(AndroidDriver<MobileElement> driver) 
	{
	
		driver.findElement(By.xpath("//*[@text='License']")).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
}
