package com.flipcart.pages;



import org.openqa.selenium.By;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class LogoutPage {
	
	public void clickOnImageButton(AndroidDriver<MobileElement> driver) {
		driver.findElement(By.className("android.widget.ImageButton")).click();
	}
	
	public void clickOnMyAccount(AndroidDriver<MobileElement> driver) {
		driver.findElement(By.xpath("//*[@text='My Account']")).click();
	}
	public void clickOnLogout(AndroidDriver<MobileElement> driver) {

		driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"Logout of this app\"))");
		
		driver.findElement(By.xpath("//*[@text='Logout of this app']")).click();
		
		driver.findElement(By.xpath("//*[@text='Yes']")).click();
	}
	
}
