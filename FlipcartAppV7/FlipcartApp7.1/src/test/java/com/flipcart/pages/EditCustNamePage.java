package com.flipcart.pages;

import org.openqa.selenium.By;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class EditCustNamePage {
	

	public void clickOnImageButton(AndroidDriver<MobileElement> driver) 
	{
		driver.findElement(By.className("android.widget.ImageButton")).click();
	}
	
	public void clickOnMyAccount(AndroidDriver<MobileElement> driver) 
	{
		driver.findElement(By.xpath("//*[@text='My Account']")).click();
	}
	
	public void clickAccountSetings(AndroidDriver<MobileElement> driver) 
	{	
		driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"Account Settings\"))");
		
		driver.findElement(By.xpath("//*[@text='Account Settings']")).click();
	}


	public void putFirstName(AndroidDriver<MobileElement> driver,String firstname) 
	{
		MobileElement fname = driver.findElement(By.className("android.widget.EditText"));
		fname.click();
		fname.clear();
		fname.sendKeys(firstname);
		
	}
	
	public void putLastName(AndroidDriver<MobileElement> driver,String lastname) 
	{
		MobileElement lname = driver.findElement(By.xpath("//*[@text='Katkar']")); 
	    lname.click();
	    lname.clear();  
	    lname.sendKeys(lastname);
	}
	
	public void clickOnSubmit(AndroidDriver<MobileElement> driver) 
	{
		driver.findElement(By.xpath("//*[@text='Submit']")).click(); 	
	}
	
}
